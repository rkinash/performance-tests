import tornado.ioloop
import tornado.web

big_response = '+'*64000


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world")


class BigResponseHandler(tornado.web.RequestHandler):
    def get(self):
        self.write(big_response)


application = tornado.web.Application([
    (r"/", MainHandler),
    (r"/big", BigResponseHandler),
])

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
